import React from 'react';
import { shallow } from 'enzyme';
import Answer from '../../../src/components/Answer';


describe('Answer Componentt', () => {

	let enzymeWrapper;

	const classNames = 'button';
	const onClick = jest.fn();
	const value = 'Test button';

	beforeEach(() => {
		enzymeWrapper = shallow(<Answer classNames={classNames} onClick={() => onClick()} value={value} />);
	})

	it('should render self', () => {

		expect(enzymeWrapper.find('button').exists()).toBe(true)
		
    expect(enzymeWrapper.find('button').hasClass(classNames)).toBe(true)
  })
})