import React from 'react';
import { shallow } from 'enzyme';
import ResetChoices from '../../../src/components/ResetChoices';


describe('ResetChoices Component', () => {

	let enzymeWrapper;

	const correctChoicesIs0 = 0;
	const correctChoicesIs5 = 5;
	const onClick = jest.fn();

	it('should render self when the correctChoices is equal to 5', () => {
		enzymeWrapper = shallow(<ResetChoices correctChoices={correctChoicesIs5} resetChoices={onClick}/>);
    
    expect(enzymeWrapper.find('button').exists()).toBe(true)

    expect(enzymeWrapper.find('button').hasClass('button')).toBe(true)
  })

  it('should render null when the correctChoices is equal to 0', () => {
		enzymeWrapper = shallow(<ResetChoices correctChoices={correctChoicesIs0} resetChoices={onClick}/>);
    
    expect(enzymeWrapper.find('button').exists()).toBe(false)
  })
})