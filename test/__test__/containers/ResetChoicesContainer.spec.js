import React from 'react';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import ResetChoicesContainer from '../../../src/components/ResetChoices';

describe('ResetChoicesContainer', () => {

	let enzymeWrapper, store;

	const initialState = {
		questionAnswers: {
			correctChoices: 5
		}
	}

	const mockStore = configureStore();
	const onClick = jest.fn();

	beforeEach(() => {
		store = mockStore(initialState)
		enzymeWrapper = shallow(
			<Provider store={store}>
				<ResetChoicesContainer correctChoices={initialState.questionAnswers.correctChoices} resetChoices={onClick} />
			</Provider>
		);
	})

	it('should render self', () => {
    expect(enzymeWrapper.find(ResetChoicesContainer).length).toEqual(1)
  }) 

  it('should have the same prop values', () => {
    expect(enzymeWrapper.prop('correctChoices')).toEqual(initialState.questionAnswers.correctChoices)
  })
})