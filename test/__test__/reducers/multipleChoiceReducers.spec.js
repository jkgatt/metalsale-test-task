import questionAnswers from '../../../src/reducers/questionAnswers';
import { SELECT_CHOICE, SUBMIT_CHOICES, RESET_CHOICES } from '../../../src/actions/actions';

// Importing mock question data
import mockQuestionData from '../../__mock__/questionData.mock.js';

describe('questionAnswers reducer', () => {
	it('questionAnswers should start the initial state', () => {
		expect(questionAnswers(undefined, {})).toEqual(
				{
					correctChoices: 0,
					wrongChoices: 0,
					choices: []
				}
			)
	})

	/*
	 * SELECT_CHOICE
	 */

	it('questionAnswers should handle SELECT_CHOICE (New Choice)', () => {
		expect(
			questionAnswers(
				{
					choices: []
				},
				{
					type: SELECT_CHOICE,
					question: 1,
					choice: 2
				}
			)
		).toEqual(
			{
					choices: [
						{
							question: 1,
							choice: 2
						}
					]
			}
		)
	})

	it('questionAnswers should handle SELECT_CHOICE (Two New Choices)', () => {
		// First new choice
		expect(
			questionAnswers(
				{
					choices: []
				},
				{
					type: SELECT_CHOICE,
					question: 1,
					choice: 2
				}
			)
		).toEqual(
			{
					choices: [
						{
							question: 1,
							choice: 2
						}
					]
			}
		)

		// Second new Choice
		expect(
			questionAnswers(
				{
					choices: [
						{
							question: 1,
							choice: 2
						}
					]
				},
				{
					type: SELECT_CHOICE,
					question: 2,
					choice: 3
				}
			)
		).toEqual(
			{
					choices: [
						{
							question: 1,
							choice: 2
						},
						{
							question: 2,
							choice: 3
						}
					]
			}
		)
	})

	it('questionAnswers should handle SELECT_CHOICE (One choice changed)', () => {
		expect(
			questionAnswers(
				{
					choices: [
						{
							question: 1,
							choice: 2
						},
						{
							question: 2,
							choice: 3
						}
					]
				},
				{
					type: SELECT_CHOICE,
					question: 1,
					choice: 4
				}
			)
		).toEqual(
			{
					choices: [
						{
							question: 1,
							choice: 4
						},
						{
							question: 2,
							choice: 3
						}
					]
			}
		)
	})

		/*
	 * SUBMIT_CHOICES
	 */
	 it('questionAnswers should handle SUBMIT_CHOICES (The number of choices, is less than the number of questions)', () => {
		expect(
			questionAnswers(
				{
					correctChoices: 0,
					wrongChoices: 0,
					choices: [
						{
							question: 1,
							choice: 2
						}
					]
				},
				{
					type: SUBMIT_CHOICES,
					questionData: mockQuestionData()
				}
			)
		).toEqual(
			{
				correctChoices: 0,
				wrongChoices: 0,
				choices: [
					{
						question: 1,
						choice: 2
					}
				]
			}
		)
	})

	it('questionAnswers should handle SUBMIT_CHOICES (All questions have been answered correctly)', () => {
		expect(
			questionAnswers(
				{
					correctChoices: 0,
					wrongChoices: 0,
					choices: [
						{
							question: 1,
							choice: 2
						},
						{
							question: 2,
							choice: 3
						}
					]
				},
				{
					type: SUBMIT_CHOICES,
					questionData: mockQuestionData()
				}
			)
		).toEqual(
			{
				correctChoices: 2,
				wrongChoices: 0,
				choices: [
					{
						question: 1,
						choice: 2
					},
					{
						question: 2,
						choice: 3
					}
				]
			}
		)
	})

	it('questionAnswers should handle SUBMIT_CHOICES (All questions have been answered wrongly.)', () => {
		expect(
			questionAnswers(
				{
					correctChoices: 0,
					wrongChoices: 0,
					choices: [
						{
							question: 1,
							choice: 1
						},
						{
							question: 2,
							choice: 4
						}
					]
				},
				{
					type: SUBMIT_CHOICES,
					questionData: mockQuestionData()
				}
			)
		).toEqual(
			{
				correctChoices: 0,
				wrongChoices: 2,
				choices: [
					{
						question: 1,
						choice: 1
					},
					{
						question: 2,
						choice: 4
					}
				]
			}
		)
	})


	/*
	 * RESET_CHOICES
	 */
	it('questionAnswers should handle RESET_CHOICES', () => {
		expect(
			questionAnswers(
				{
					correctChoices: 1,
					wrongChoices: 1,
					choices: [
						{
							question: 1,
							choice: 4
						},
						{
							question: 2,
							choice: 3
						}
					]
				}, 
				{type: RESET_CHOICES}
			)).toEqual(
				{
					correctChoices: 0,
					wrongChoices: 0,
					choices: []
				}
			)
	})
})