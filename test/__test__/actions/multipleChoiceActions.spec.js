import { selectChoice, submitChoices, resetChoices } from '../../../src/actions/actions';
import { SELECT_CHOICE, SUBMIT_CHOICES, RESET_CHOICES } from '../../../src/actions/actions';

// Importing mock question data
import mockQuestionData from '../../__mock__/questionData.mock.js';

describe('actions', () => {
	it('selectChoice should create a SELECT_CHOICE action', () => {
		const questionIndex = 1;
		const answerIndex = 1;
		const expectedAction = {
			type: SELECT_CHOICE,
			question: questionIndex,
			choice: answerIndex
		}
		expect(selectChoice(questionIndex, answerIndex)).toEqual(expectedAction)
	})

	it('submitChoices should create a SUBMIT_CHOICES action', () => {

		const answerIndex = 1;
		const expectedAction = {
			type: SUBMIT_CHOICES,
			questionData: mockQuestionData()
		}
		expect(submitChoices(mockQuestionData())).toEqual(expectedAction)
	})	

	it('resetChoices should create a RESET_CHOICES action', () => {
		const expectedAction = {
			type: RESET_CHOICES
		}
		expect(resetChoices()).toEqual(expectedAction)
	})	
})