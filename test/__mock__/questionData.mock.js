export default function mockQuestionData() {
	return [
		{
			id: 1,
			question: "Iron, cobalt and nickel are metals which are...",
			answers:[
				"non-magnetic",
				"magnetic",
				"insulator",
				"none of them"
			],
			correct: 2
		},
		{
			id: 2,
			question: "Elements that are brittle and cannot be rolled into wires are known as...",
			answers:[
				"liquid",
				"non-elastic",
				"non-metal",
				"metal"
			],
			correct: 3
		}
	]
}