import React from 'react';
import ReactDOM from 'react-dom';

// Store
import { createStore } from 'redux';
import allReducers from './reducers/reducers';

// Provider
import { Provider } from 'react-redux';

import style from './style.scss';

import Main from './components/Main.jsx';

let store = createStore(allReducers);

ReactDOM.render(
	<Provider store={store}>
		<Main />
	</Provider>,
	document.getElementById('app')
);

/*

// TESTING STORE

import questionData from './reducers/questionData';

import {
	selectChoice,
	submitChoices,
	resetChoices
} from './actions/actions';

// Log the initial state
console.log(store.getState());

// Every time the state changes, log it
// Note that subscribe() returns a function for unregistering the listener
let unsubscribe = store.subscribe(() =>
  console.log(store.getState())
)

store.dispatch(selectChoice(1, 2));
store.dispatch(selectChoice(2, 2));
store.dispatch(submitChoices(questionData()));
store.dispatch(resetChoices());

// Stop listening to state updates
unsubscribe()
*/