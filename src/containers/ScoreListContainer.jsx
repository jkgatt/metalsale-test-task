import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import ScoreList from '../components/ScoreList.jsx';

const mapStateToProp = (state) => {
	return {
		correctChoices: state.questionAnswers.correctChoices,
		wrongChoices: state.questionAnswers.wrongChoices
	}
}

const ScoreListContainer = connect (
	mapStateToProp
)(ScoreList);

export default ScoreListContainer;