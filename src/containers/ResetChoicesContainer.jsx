import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { resetChoices } from '../actions/actions';

import ResetChoices from '../components/ResetChoices.jsx';

const mapStateToProp = (state) => {
	return {
		correctChoices: state.questionAnswers.correctChoices
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
			resetChoices: resetChoices
		}, 
		dispatch
	);
}

const ResetChoicesContainer = connect (
	mapStateToProp,
	mapDispatchToProps
)(ResetChoices);

export default ResetChoicesContainer;