import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { selectChoice } from '../actions/actions';

import MultipleChoiceQuestions from '../components/MultipleChoiceQuestions.jsx';

const mapStateToProp = (state) => {
	return {
		questionData: state.questionData,
		questionAnswers: state.questionAnswers
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
			selectChoice: selectChoice
		}, 
		dispatch
	);
}

const MultipleChoiceQuestionsContainer = connect (
	mapStateToProp,
	mapDispatchToProps
)(MultipleChoiceQuestions);

export default MultipleChoiceQuestionsContainer;