import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { submitChoices } from '../actions/actions';

import SubmitChoices from '../components/SubmitChoices.jsx';

const mapStateToProp = (state) => {
	return {
		questionData: state.questionData,
		submittedChoices: state.questionAnswers.choices.length
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
			submitChoices: submitChoices
		}, 
		dispatch
	);
}

const SubmitChoicesContainer = connect (
	mapStateToProp,
	mapDispatchToProps
)(SubmitChoices);

export default SubmitChoicesContainer;