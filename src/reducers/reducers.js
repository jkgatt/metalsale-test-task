import { combineReducers } from 'redux';

// Reducers
import questionData from './questionData';
import questionAnswers from './questionAnswers';

const allReducers = combineReducers({
	questionAnswers: questionAnswers,
	questionData: questionData
});

export default allReducers;