import { SELECT_CHOICE, SUBMIT_CHOICES, RESET_CHOICES } from '../actions/actions'

// Initial State
const initialState = {
	correctChoices: 0,
	wrongChoices: 0,
	choices: []
};	

export default function questionAnswers(state = initialState, action)
{
	switch(action.type)
	{
		case SELECT_CHOICE:
			let questionAnswered = false;
			let editableState = Object.assign({}, state);
			for(let currentChoice in editableState.choices) {
				if(action.question === editableState.choices[currentChoice].question){
					editableState.choices[currentChoice].choice = action.choice;
					questionAnswered = true;
					break;
				}
			}
			if(questionAnswered)
			{	
				return Object.assign({}, state, editableState)
			} else {
				return Object.assign({}, state,
				{
					choices: [
						...state.choices,
						{
							question: action.question,
							choice: action.choice
						}
					]
				});
			}
		case SUBMIT_CHOICES:
			let correctChoices = 0;
			let wrongChoices = 0;

			if(state.choices.length === action.questionData.length) {
				state.choices.map((choice, index) => {
					if(action.questionData[choice.question - 1].correct === choice.choice)
					{
						correctChoices++;
					} else {
						wrongChoices++;
					}
				});

				return Object.assign({}, state,
					{
						correctChoices: correctChoices, 
						wrongChoices: wrongChoices
					});
			}
			return state;
		case RESET_CHOICES:
			return state = initialState;
		default:
			return state;
	}
}