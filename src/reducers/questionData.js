export default function questionData() {
	return [
		{
			id: 1,
			question: "Iron, cobalt and nickel are metals which are...",
			answers:[
				"non-magnetic",
				"magnetic",
				"insulator",
				"none of them"
			],
			correct: 2
		},
		{
			id: 2,
			question: "Elements that are brittle and cannot be rolled into wires are known as...",
			answers:[
				"liquid",
				"non-elastic",
				"non-metal",
				"metal"
			],
			correct: 3
		},
		{
			id: 3,
			question: "German silver is an alloy of...",
			answers:[
				"copper,nickel and silver",
				"silver, copper and aluminium",
				"zinc, copper and nickel",
				"silver, zinc and copper"
			],
			correct: 3
		},
		{
			id: 4,
			question: "The element common to all acids is...",
			answers:[
				"carbon",
				"hydrogen",
				"oxygen",
				"sulphur"
			],
			correct: 2
		},
		{
			id: 5,
			question: "What is laughing gas ?",
			answers:[
				"Carbon dioxide",
				"Sulphur dioxide",
				"Hydrogen peroxide",
				"Nitrous oxide"
			],
			correct: 4
		},
		{
			id: 6,
			question: "Which of the following is the lightest metal?",
			answers:[
				"Mercury",
				"Silver",
				"Lithium",
				"Lead"
			],
			correct: 3
		},
		{
			id: 7,
			question: "The most important ore of aluminium is...",
			answers:[
				"bauxite",
				"calamine",
				"calcite",
				"galena"
			],
			correct: 1
		},
		{
			id: 8,
			question: "Diamond is an allotropic from of...",
			answers:[
				"silicon",
				"carbon",
				"sulphur",
				"germanium"
			],
			correct: 2
		}
	]
}