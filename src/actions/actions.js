/*
 *	Action Types
 */
export const SELECT_CHOICE = 'SELECT_CHOICE'
export const SUBMIT_CHOICES = 'SUBMIT_CHOICES'
export const RESET_CHOICES = 'RESET_CHOICES'

/*
 *	Action Creators
 */

// Dispatched when selecting a choice/answer
export function selectChoice(questionIndex, choiceIndex)
{
	return {
		type: SELECT_CHOICE,
		question: questionIndex,
		choice: choiceIndex
	}
};

// Dispatched when all questions have been answered
export function submitChoices(questionData)
{
	return {
		type: SUBMIT_CHOICES,
		questionData: questionData
	}
};

// Dispatched when
export function resetChoices()
{
	return {
		type: RESET_CHOICES
	}
};