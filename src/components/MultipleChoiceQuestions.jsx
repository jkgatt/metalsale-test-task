import React from 'react';
import PropTypes from 'prop-types';

import Answer from './Answer.jsx';

export default class MultipleChoiceQuestions extends React.Component{

	evalClassNames(questionId, answerId, choices){
		for(let currentChoice in choices) {
			if(questionId === choices[currentChoice].question &&
			 answerId === choices[currentChoice].choice){
				return "answer chosen";
			}
		}
		return "answer";
	}

	createAnswerList(questionAnswer) {
		return questionAnswer.answers.map((answer, index) => {
			let answerId = index + 1;
			return(
				<Answer 
					key={answerId} 
					value={answer} 
					onClick={() => this.props.selectChoice(questionAnswer.id, answerId)}
					classNames= {
						this.evalClassNames(
							questionAnswer.id, 
							answerId, 
							this.props.questionAnswers.choices
						)
					}
				/>
			);
		})
	}

	createQuestionAnswerList() {
		if(this.props.questionAnswers.correctChoices === 0) 
		{
			return this.props.questionData.map((questionAnswer) => {
				return (
					<div key={questionAnswer.id} className="multiple-choice-container">
						<p className="question">{questionAnswer.question}</p>
						{this.createAnswerList(questionAnswer)}
					</div>
				);
			})
		} else {
			return null;
		}
	}
	
	render() {
		return (
			<div>
					{this.createQuestionAnswerList()}
			</div>
		);
	}
}

MultipleChoiceQuestions.propTypes = {
	questionData: PropTypes.arrayOf(
		PropTypes.shape ({
			id: PropTypes.number.isRequired,
			question: PropTypes.string.isRequired,
			answers: PropTypes.arrayOf(PropTypes.string).isRequired
		}).isRequired
	).isRequired,
};