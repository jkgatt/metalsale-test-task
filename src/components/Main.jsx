import React from 'react';

import MultipleChoiceQuestionsContainer from '../containers/MultipleChoiceQuestionsContainer.jsx';
import SubmitChoicesContainer from '../containers/SubmitChoicesContainer.jsx';
import ScoreListContainer from '../containers/ScoreListContainer.jsx';
import ResetChoicesContainer from '../containers/ResetChoicesContainer.jsx';

export default class Main extends React.Component {
	render() {
		return (
			<div>
				<h1>MetalSale Task</h1>
				<MultipleChoiceQuestionsContainer />
				<ScoreListContainer />
				<SubmitChoicesContainer />
				<ResetChoicesContainer />
			</div>
		)
	};
}