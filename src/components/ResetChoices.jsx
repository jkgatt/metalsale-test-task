import React from 'react';
import PropTypes from 'prop-types';

const ResetChoices = (props) => {
		if(props.correctChoices !== 0) 
		{
			return (
				<button 
				className="button"
				onClick={() => props.resetChoices()}
				>
					Want to take the test again?
				</button>
				);
		} else {
			return null;
		}
}

ResetChoices.propTypes = {
	correctChoices: PropTypes.number.isRequired,
	resetChoices: PropTypes.func.isRequired
}

export default ResetChoices;