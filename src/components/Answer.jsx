import React from 'react';
import PropTypes from 'prop-types';

const Answer = (props) => {
	return (
		<button className={props.classNames} onClick={props.onClick}>
			{props.value}
		</button>
	);
};

Answer.propTypes = {
	classNames: PropTypes.string.isRequired,
	onClick: PropTypes.func.isRequired,
	value: PropTypes.string.isRequired
};

export default Answer;