import React from 'react';
import PropTypes from 'prop-types';

export default class SubmitChoices extends React.Component {

	evalClassNames(){
		if(this.props.questionData.length === this.props.submittedChoices)
		{
			return "button";
		}
		return "button disabled";
	}

	render() {
		return (
			<button 
				className={this.evalClassNames()} 
				onClick={() => this.props.submitChoices(this.props.questionData)}
			>
				Check Answers
			</button>
		);
	}
}

SubmitChoices.propTypes = {
	submitChoices: PropTypes.func.isRequired,
	questionData: PropTypes.array.isRequired,
	submittedChoices: PropTypes.number.isRequired
};