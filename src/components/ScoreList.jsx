import React from 'react';
import PropTypes from 'prop-types';

const ScoreList = (props) => {
		if(props.correctChoices !== 0) 
		{
			return (
				<div className="score-list">
					<h2>Score</h2>
					<p>Correct Answers: {props.correctChoices}</p>
					<p>Wrong Answers: {props.wrongChoices}</p>
				</div>
			);
		} else {
			return null;
		}
}

ScoreList.propTypes = {
	correctChoices: PropTypes.number.isRequired,
	wrongChoices: PropTypes.number.isRequired
};

export default ScoreList;