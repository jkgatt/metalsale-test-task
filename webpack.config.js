const path = require('path');

// Used to extract the css into its own file
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ExtractTextPluginConfig = new ExtractTextPlugin({
	filename: 'style.css'
});

// Used to automatically add bundle.js file name to index.html
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
	template: './src/index.html',
	filename: 'index.html',
	inject: 	'body' 
});

module.exports = {
	entry: [
		'./src/index.jsx'
	],
	output: {
		path: path.resolve(__dirname, 'public'),
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules)/,
				use: [{
					loader: 'babel-loader',
					options: {
						'presets':[
							'es2015',		// Used to transform es6 code to vanilla JS
							'react'			// Used to transfrom .JSX to .JS
						]
					}
				}]
			},
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					use: [
						'css-loader', 	// Used to pass CSS to the JS
						'sass-loader'		// Used to compile SCSS/SASS to CSS
						],
					fallback: 'style-loader'
				})
			}
		]
	},
	plugins: [
		ExtractTextPluginConfig,
		HtmlWebpackPluginConfig
	],
	devServer: {
		inline: true,
		contentBase: './public',
		port: 3000
   }
}