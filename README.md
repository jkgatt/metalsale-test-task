# MetalSale Test Task

## Instructions by Gerrit Mewes

Please write a React application. Use redux as state container. Do not develop any backend.
The UI should do a multiple-choice test with the user with 8 questions. Afterwards it should
tell the user how many answers have been right and how many have been wrong. At the end,
it should give the user the option to answer the test again.
Write automated tests to ensure that the application works as expected.
Please describe how to use your application in a README.md file.

## Getting Started
First the repository must be downloaded or cloned and then the necessary dependencies need to be installed.
```
> npm install
```

Next an initial webpack build should be run.
```
> npm run compile
```

The webpack development server should then be started.
```
> npm run start
```

To view your project, go to: [http://localhost:3000/](http://localhost:3000/)